# pref.js
![pref.js Logo](images/logo.png)
## What is pref.js?
pref.js is a JavaScript framework that pre-fetches all your pages to prevent long loading times. The framework itself is also very lightweight (only 96 lines of code!).
## How to use pref.js
To use pref.js all you have to do is to include the `pref.js` or `pref.min.js` to your project. That's it!
## Adjust the order
You can adjust the order which pages are fetched first with the `pref-importance` attribute. The higher the given number, the faster the page is pre-fetched.
## Other
- You **can** import the script only once in your main page, but you **should** include it on every page.
- The `<head>` is not modified. The only modified element is the `<body>` element.