window.addEventListener('load', () => {
    if(window.attachedPref === undefined) {
        todos = [];
        fetched = [];

        window.attachedPref = true;

        let pref = (d) => {
            [...d.getElementsByTagName('a')].forEach((e, i) => {
                if(e.href !== undefined) {
                    if(e.href !== '#') {
                        const h = e.href;

                        e.addEventListener('click', () => {
                            navigate(h);
                        });

                        if(e.getAttribute('pref-importance') !== undefined) {
                            todos.push([parseInt(e.getAttribute('pref-importance')), e.href]);
                        } else {
                            todos.push([0, e.href]);
                        }

                        e.href = '#';
                        d.getElementsByTagName('a')[i].replaceWith(e);
                    }
                }
            });

            return d;
        }

        window.navigate = (l) => {
            let el;
            let ti;

            for(let i = 0; i < fetched.length; i++) {
                let f = fetched[i];

                if(f[1] === l) {
                    el = fetched[i][0];
                    ti = fetched[i][2];
                }
            }

            if(el === undefined) {
                prefetch(l);
                el = fetched[0][0];
                ti = fetched[0][2];
            }

            window.history.pushState('', '', l);
            document.title = ti;
            document.body = el;
        };

        prefetch = (t) => {
            let fo = false;

            for(let i = 0; i < fetched.length; i++) {
                if(fetched[i][1] === t) {
                    fo = true;
                }
            }

            if(!fo) {
                let r = new XMLHttpRequest();

                r.addEventListener('load', () => {
                    if(r.status == 200) {
                        fetched.push([pref(new DOMParser().parseFromString(r.responseText, 'text/html')).getElementsByTagName('body')[0], t, new DOMParser().parseFromString(r.responseText, 'text/html').title]);
                    }
                });

                r.open('GET', t);
                r.send();
            }
        };

        document = pref(document);

        setInterval(() => {
            if(todos.length > 0) {
                todos.sort((a, b) => a[1].localeCompare(b[1])).reverse();
                todos.forEach((t) => {
                    prefetch(t[1]);
                    todos.splice(0, 1)
                });
            }
        });
    }
});